library("reshape2")
library("ggplot2")
library("scales")

# average gross salary taken from https://en.wikipedia.org/wiki/List_of_European_countries_by_average_wage#National_sources

tax_data <- data.frame(
  income = c(0.5, 0.75, 1, 1.25, 1.5, 1.75, 2),
  # https://www.securex.eu/advice/Simulations/Public/BrutNet/BerekenBrutNet.aspx
  Belgium = 1 - c(1220/(1631+533), 1597/(2446+800), 1961/(3261+1066), 2319/(4076+1333), 2653/(4891+1599), 2986/(5707+1866), 3313/(6522+2133)),
  # http://www.ekonomifakta.se/sv/Fakta/Skatter/Rakna-pa-dina-skatter/Rakna-ut-din-skatt/ (Stockholm, no church tax, born 1980)
  Sweden = 1 - c(12629/20107, 18247/30161, 23845/40214, 28950/50268, 32786/60322, 36566/70375, 40019/80429),
  # http://colawtax.hu/en/calculators/net-salary-calculator/
  Hungary = 1 - c(79484/155935, 119233/233915, 158696/311870, 198711/389837, 238453/467804, 278195/545772, 317937/623739),
  # http://www.platy.sk/en/calculator/
  Slovakia = 1 - c(389/598, 543/931, 704/1241, 865/1551, 1026/1862, 1187/2172, 1348/2482),
  # http://colawtax.hu/en/calculators/net-salary-calculator/
  CzechRepublic = 1 - c(11608/18600, 16378/27900, 21149/37200, 25918/46500, 30702/55700, 35473/65000, 40243/74300),
  # http://www.bankier.pl/narzedzia/kalkulator-placowy
  Poland = 1 - c(1474/2436, 2177/3655, 2881/4873, 3585/6091, 4289/7309, 4993/8527, 5697/9745),
  # http://www.uktaxcalculators.co.uk/
  UnitedKingdom = 1 - c(1174/1441, 1632/2207, 2091/2975, 2549/3742, 2956/4509, 3347/5276, 3738/6043),
  # https://www.ethz.ch/en/the-eth-zurich/welcome-center/services-and-downloads/salary-calculator.html (Zurich, 35 yrs, church member: no)
  Switzerland = 1 - c(2749/3220, 4007/4829, 5208/6439, 6361/8049, 7440/9659, 8490/11268, 9490/12878)
)



tax_data_long <- melt(tax_data, id="income")  # convert to long format

ggplot(data=tax_data_long,
  aes(x=income, y=value, colour=variable)) +
  geom_line(size=1) +
  scale_x_continuous(labels = percent, name="income (in percent of mean salary in given country)") +
  scale_y_continuous(name="tax wedge", limits=c(0,0.65), labels=percent) +
  theme(legend.title=element_blank()) +
  ggtitle("Tax wedge for a single worker")